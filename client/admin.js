Template.admin.helpers({
	images: function() {
		return Images.find();
	}
});

Template.admin.events({
	'click #addImage': function(event, template) {
		$("#fileInput").click();
	},
	'change #fileInput': function(event, template) {
		var files = event.target.files;
		for (var i = 0, ln = files.length; i < ln; i++) {
			Images.insert(files[i], function (err, fileObj) {
			});
		}
	},
	'click .delImage': function(e, t) {
		var imageId = this._id;
		Images.remove(imageId);
	}
});